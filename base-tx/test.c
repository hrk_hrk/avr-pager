/* Copyright 2014, Kenneth MacKay. Licensed under the BSD 2-clause license. */

#include "../libs/micro-ecc/uECC.h"
//#include "../include/oaes_lib.h"
//#include "mbedtls_config.h"
//#include "../libs/mbedtls-2.2.0/include/mbedtls/config.h"
//#include "../libs/mbedtls-2.2.0/include/mbedtls/aes.h"
//#include "../libs/mbedtls-2.2.0/library/aes.c"
//#include <gnutls/gnutls.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//#include "packets.h"
#include "../libs/pagerlib/pagerlib.h"
#ifndef uECC_TEST_NUMBER_OF_ITERATIONS
#define uECC_TEST_NUMBER_OF_ITERATIONS   1
#endif

char shared_secret[SHARED_SECRET_SIZE];
char decompressed_point[64];



int main() {
    struct pl_keypair  *receiver, *sender;
    char clear_message[] = "Blaat blaat, dit is een test berichtje :) ";
    char crypt_message[128];
    char decrypted_message[128];
//    struct pl_pagermessage * sended_msg; 
    // initialise the pager
    struct pl_ctx * context;
/*
    if ( pl_init(context) < 1 )
    {
      DBG("pagerlib initialized \n");
    }
*/
    context = pl_init(context);
//    sended_msg = (struct pl_pagermessage *) malloc(sizeof(struct pl_pagermessage));

//    memset(sended_msg, 7, sizeof(struct pl_pagermessage));
    sender = (struct pl_keypair *) malloc(sizeof(struct pl_keypair));
    memset(sender, 9, sizeof(struct pl_keypair));

    receiver = (struct pl_keypair *)  malloc(sizeof(struct pl_keypair));
    memset(receiver, 2, sizeof(struct pl_keypair));


    /* create keypairs */
    /*
    pl_create_keypair(context, sender);
    pl_save_key(sender,"sender.keypair");
    pl_create_keypair(context, receiver);
    pl_save_key(receiver,"receiver.keypair");
    */
    //switch to sender key
//    context->kp = sender;
    pl_load_key(sender, "sender.keypair");
    pl_load_key(receiver, "receiver.keypair");
    context->kp = sender;
    DBG("\n sender.keypair");
    DBM("sender", sizeof(struct pl_keypair), sender);
    DBM("receiver", sizeof(struct pl_keypair), receiver);
    DBM("context->kp (sender)", sizeof(struct pl_keypair), context->kp);
    DBM("context->kp->private_key (sender)", sizeof(context->kp->private_key), &context->kp->private_key);



    memcpy(context->msg->msg, clear_message, MSG_SIZE);

    DBM("receiver->compressed",sizeof(receiver->compressed_point) , &receiver->compressed_point);

    memcpy(&context->receiver_compressed_point, &receiver->compressed_point, sizeof(context->receiver_compressed_point));
    DBM("context->receiver_compressed_point",sizeof(context->receiver_compressed_point) , &context->receiver_compressed_point);

    pl_send_message(context);

    context->kp = receiver;
    DBM("context->kp (receiver)", sizeof(struct pl_keypair), context->kp);
    DBM("context->kp->private_key (receiver)", sizeof(context->kp->private_key), &context->kp->private_key);


    pl_receive_message(context);

    printf("the decrypted message: %s \n", context->msg->msg);
   // printf("\n ----------------------------\n");

    return 0;
}
