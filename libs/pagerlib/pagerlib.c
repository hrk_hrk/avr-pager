#include <stdio.h>
#include <string.h>
#include "../micro-ecc/uECC.h"
#include "packets.h"
#include <stdlib.h>
#include "things.h"
#include "pagerlib.h"
       #include <sys/types.h>
       #include <sys/stat.h>
       #include <fcntl.h>

unsigned char fakeiv[] = "123456789abcdefghijklmnop";

int rng (uint8_t *dest, unsigned size) {
#ifndef ARDUINO
  int fd = open("/dev/urandom", O_RDONLY);
  if (fd == -1) {
    fd = open("/dev/random", O_RDONLY);
    if (fd == -1) {
      return 0;
    }
  }

  char *ptr = (char *)dest;
  size_t left = size;
  while (left > 0) {
    size_t bytes_read = read(fd, ptr, left);
    if (bytes_read <= 0) { // read failed
      close(fd);
      return 0;
    }
    left -= bytes_read;
    ptr += bytes_read;
  }

  close(fd);
  return 1;
#else
  return 0;
#endif
}



inline struct pl_ctx * pl_init() {
  struct pl_ctx * ctx;

  ctx = (struct pl_ctx *) malloc(sizeof(struct pl_ctx));

  ctx->curve = CURVE;

  ctx->msg = (struct pl_pagermessage *) malloc(sizeof(struct pl_pagermessage));
  memset(ctx->msg, 7, sizeof(struct pl_pagermessage));

  ctx->kp = (struct pl_keypair *) malloc(sizeof(struct pl_keypair));
  memset(ctx->kp, 9, sizeof(struct pl_keypair));

  uECC_set_rng(rng);
  return ctx;
}

inline int pl_set_receiver(struct pl_ctx *ctx, struct pl_keypair *keypair)
{

  return 1; 
}

/*
 * This sends a message (encrypts it)
 * Before calling this make sure u set sender ( ctx->kp , ctx->msg->sender_compressed_point) and receiver ctx->receiver_compressed_point, and the message you want to send
 */

inline int pl_send_message(struct pl_ctx *ctx) {

  memset(ctx->shared_secret, 7, sizeof(ctx->shared_secret));

  /* create a random iv */ 
  // TODO (this is stupid)

//  memcpy(&ctx->msg->iv, &fakeiv, sizeof(ctx->msg->iv));
  rng(&ctx->msg->iv, sizeof(ctx->msg->iv));
  // copy compressed point into pager message
  memcpy(&ctx->msg->sender_compressed_point, &ctx->kp->compressed_point, sizeof(ctx->msg->sender_compressed_point));

  //DBG("Sending message \n");
#ifndef NOCRYPT
  /* decompress key */
  uECC_decompress(&ctx->receiver_compressed_point, &ctx->decompressed_point, ctx->curve);
  
  DBM("#receiver compressed point ",sizeof(ctx->receiver_compressed_point) , &ctx->receiver_compressed_point);
  DBM("#decompr point ",sizeof(ctx->decompressed_point) , &ctx->decompressed_point);


  /*calculate shared secret on sender*/
  
  if (!uECC_shared_secret(&ctx->decompressed_point, &ctx->kp->private_key, &ctx->shared_secret, ctx->curve)) {
    DBG("shared_secret() failed in send_message (1)\n");
    return 1;
  }
  
  DBM("msg->msg in pl_send_message before crypt", sizeof(ctx->msg->msg), &ctx->msg->msg);
  DBM("shared secret in pl_send_message", sizeof(ctx->shared_secret), &ctx->shared_secret);

#ifdef ARDUINO
  
     ctx->aes_ctx = aes192_cbc_enc_start(&ctx->shared_secret, ctx->msg->iv);
     aes192_cbc_enc_continue(ctx->aes_ctx, ctx->msg->msg, MSG_SIZE);
     aes192_cbc_enc_finish(ctx->aes_ctx);
 
#else
  mbedtls_aes_init( &ctx->aes_ctx );

  /* encrypt message with aes using shared secret as the key */
  mbedtls_aes_setkey_enc( &ctx->aes_ctx, &ctx->shared_secret, AES_KEYSIZE );

  char cmsg[MSG_SIZE];
  memcpy(&cmsg, &ctx->msg->msg, MSG_SIZE);
  char civ[IV_SIZE];
  memcpy(&civ, &ctx->msg->iv, IV_SIZE);

  DBM("iv in pl_send_message before crypt", sizeof(ctx->msg->iv), &ctx->msg->iv);


  mbedtls_aes_crypt_cbc( &ctx->aes_ctx, MBEDTLS_AES_ENCRYPT, MSG_SIZE, civ, &cmsg, &ctx->msg->msg );
  mbedtls_aes_free (&ctx->aes_ctx);
//  DBG("message to send: %s \n ", ctx->msg);
#endif
#endif
  /*
     char to_base64[sizeof(struct pl_keypair)];
     base64_encode(&to_base64, to, sizeof(struct pl_keypair));
     DBG(" \nto (keypair):  len: %u base64: \n %129.129s \n", sizeof(struct pl_keypair) ,to_base64, to);

     char msg_base64[sizeof(struct pl_pagermessage)];
     base64_encode(&msg_base64, msg, sizeof(struct pl_pagermessage));
     DBG(" \n encmsg: len: %u base64: \n %177.177s \n", sizeof(struct pl_pagermessage) ,msg_base64, msg);
     */
  DBM("msg->msg in pl_send_message after crypt", sizeof(ctx->msg->msg), &ctx->msg->msg);
  DBM("iv in pl_send_message after crypt", sizeof(ctx->msg->iv), &ctx->msg->iv);



  return 0;
}
inline int pl_receive_message(struct pl_ctx * ctx)
{
  memset(&ctx->shared_secret, 7, sizeof(ctx->shared_secret));
  memset(&ctx->clear_message, 7, sizeof(ctx->clear_message));

  DBM("msg->msg in pl_receive_message before crypt", sizeof(ctx->msg->msg), &ctx->msg->msg);

  /* decompress the senders public key */
  uECC_decompress(&ctx->msg->sender_compressed_point, &ctx->decompressed_point, ctx->curve);
  DBM("ctx->kp", sizeof(struct pl_pagermessage),&ctx->kp);
  DBM("ctx->kp->private_key", sizeof(ctx->kp->private_key),&ctx->kp->private_key);
  /*calculate shared secret on receiver*/
  if (!uECC_shared_secret(&ctx->decompressed_point, &ctx->kp->private_key, &ctx->shared_secret, ctx->curve)) {
    DBG("shared_secret() failed (receive)\n");
  }
  DBM("shared secret in pl_receive_message", sizeof(ctx->shared_secret), &ctx->shared_secret);


#ifndef NOCRYPT
#ifdef ARDUINO
  memcpy(ctx->clear_message, ctx->msg->msg, MSG_SIZE);
/*
  aes_context aes_ctx;
  aes_ctx = aes192_cbc_dec_start(ctx->shared_secret, ctx->msg->iv);
  aes192_cbc_dec_continue(aes_ctx, ctx->clear_message, MSG_SIZE);
  aes192_cbc_dec_finish(aes_ctx);
 */
#else
  /* decrypt the message */
  mbedtls_aes_init( &ctx->aes_ctx );
  DBM("iv in pl_receive_message", sizeof(ctx->msg->iv), &ctx->msg->iv);

  mbedtls_aes_setkey_dec( &ctx->aes_ctx, &ctx->shared_secret, AES_KEYSIZE );

  mbedtls_aes_crypt_cbc( &ctx->aes_ctx, MBEDTLS_AES_DECRYPT, MSG_SIZE, ctx->msg->iv, &ctx->msg->msg, &ctx->clear_message );
  mbedtls_aes_free ( &ctx->aes_ctx );
  memcpy(ctx->msg->msg, ctx->clear_message, MSG_SIZE);

#endif
#endif
  DBM("msg->msg in pl_receive_message after crypt", sizeof(ctx->msg->msg), &ctx->msg->msg);
  DBM("iv in pl_receive_message after crypt", sizeof(ctx->msg->iv), &ctx->msg->iv);

}



inline int pl_create_keypair(struct pl_ctx *ctx, struct pl_keypair *keypair) {
  

//  memset(keypair->public_key, 3, sizeof(keypair->public_key));
//  memset(keypair->private_key, 4, sizeof(keypair->private_key));
//  memset(keypair->compressed_point, 5, sizeof(keypair->compressed_point));


  /* Generate arbitrary EC point (public) on Curve */
  if (!uECC_make_key(&ctx->decompressed_point, &keypair->private_key, ctx->curve)) {
    DBG("uECC_make_key() failed\n");
  }
  uECC_compress(&ctx->decompressed_point, &keypair->compressed_point, ctx->curve);
//    DBG("compress failed in create_keypair");
 
  return 0;
}

inline int
pl_save_key(struct pl_keypair *key, char * filename) {

#ifdef ARDUINO
  // not implemented
  return 0;
#else

  FILE  *sf;
  sf = fopen(filename, "w");
  fwrite(key, 1, sizeof(struct pl_keypair), sf);
  fclose(sf);
#endif

}

inline int
pl_load_key(struct pl_keypair *key, char * filename) {
#ifdef ARDUINO
  return 0;
#else
  FILE  *lf;
  lf = fopen(filename, "r");
  fread(key, 1, sizeof(struct pl_keypair), lf);

  fclose(lf);
#endif
}




