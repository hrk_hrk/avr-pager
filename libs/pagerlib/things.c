#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#if defined(__AVR_ATmega328P__)
#define ARDUINO
#endif

#ifdef ARDUINO

#include <avr/pgmspace.h>
const char PROGMEM b64_alphabet[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz"
"0123456789+/";

#else        
const char b64_alphabet[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz"
"0123456789+/";

#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"
#include "mbedtls/aes.h"


#endif
/* 'Private' declarations */
/* why?
   inline void a3_to_a4(unsigned char * a4, unsigned char * a3);
   inline void a4_to_a3(unsigned char * a3, unsigned char * a4);
   inline unsigned char b64_lookup(char c);
   */

#ifdef ARDUINO
void dump_buffer(unsigned int n, const unsigned char* buf)
{
  // not implemented
  return;
}
#else
  void
dump_buffer(unsigned int n, const unsigned char* buf)
{
  const unsigned char *p, *end;
  unsigned i, j;
  end = buf + n;

  for (i = 0; ; i += 16) {
    p = buf + i;
    for (j = 0; j < 16; j++) {
      fprintf(stderr, "%02X ", p[j]);
      if (p + j >= end)
        goto BREAKOUT;
    }
    fprintf(stderr, " ");
    p = buf + i;
    for (j = 0; j < 16; j++) {
      fprintf(stderr, "%c", isprint(p[j]) ? p[j] :
          '.');
      if (p + j >= end)
        goto BREAKOUT;
    }
    fprintf(stderr, "\n");
  }
BREAKOUT:
  return;
}
void
  dump_buffer_msg(char * msg, unsigned int n, void * buf)
{
  fprintf(stderr,"\n DBM: %s \n", msg);
  dump_buffer(n, buf);
  fprintf(stderr,"\n");
}



void vli_print(char *str, uint8_t *vli, unsigned int size) {
  //  printf("%s ", str);
  for(unsigned i=0; i<size; ++i) {
    printf("%02X ", (unsigned)vli[i]);
  }
  printf("\n");
}



#endif



static inline void a3_to_a4(unsigned char * a4, unsigned char * a3) {
  a4[0] = (a3[0] & 0xfc) >> 2;
  a4[1] = ((a3[0] & 0x03) << 4) + ((a3[1] & 0xf0) >> 4);
  a4[2] = ((a3[1] & 0x0f) << 2) + ((a3[2] & 0xc0) >> 6);
  a4[3] = (a3[2] & 0x3f);
}

static inline void a4_to_a3(unsigned char * a3, unsigned char * a4) {
  a3[0] = (a4[0] << 2) + ((a4[1] & 0x30) >> 4);
  a3[1] = ((a4[1] & 0xf) << 4) + ((a4[2] & 0x3c) >> 2);
  a3[2] = ((a4[2] & 0x3) << 6) + a4[3];
}

static inline unsigned char b64_lookup(char c) {
  if(c >='A' && c <='Z') return c - 'A';
  if(c >='a' && c <='z') return c - 71;
  if(c >='0' && c <='9') return c + 4;
  if(c == '+') return 62;
  if(c == '/') return 63;
  return -1;
}

int base64_encode(char *output, char *input, int inputLen) {
  int i = 0, j = 0;
  int encLen = 0;
  unsigned char a3[3];
  unsigned char a4[4];

  while(inputLen--) {
    a3[i++] = *(input++);
    if(i == 3) {
      a3_to_a4(a4, a3);

      for(i = 0; i < 4; i++) {
#ifdef ARDUINO
        output[encLen++] = pgm_read_byte(&b64_alphabet[a4[i]]);
#else
        output[encLen++] = b64_alphabet[a4[i]];
#endif
      }

      i = 0;
    }
  }

  if(i) {
    for(j = i; j < 3; j++) {
      a3[j] = '\0';
    }

    a3_to_a4(a4, a3);

    for(j = 0; j < i + 1; j++) {
#ifdef ARDUINO
      output[encLen++] = pgm_read_byte(&b64_alphabet[a4[j]]);
#else
      output[encLen++] = b64_alphabet[a4[j]];
#endif

    }

    while((i++ < 3)) {
      output[encLen++] = '=';
    }
  }
  output[encLen] = '\0';
  return encLen;
}

int base64_decode(char * output, char * input, int inputLen) {
  int i = 0, j = 0;
  int decLen = 0;
  unsigned char a3[3];
  unsigned char a4[4];


  while (inputLen--) {
    if(*input == '=') {
      break;
    }

    a4[i++] = *(input++);
    if (i == 4) {
      for (i = 0; i <4; i++) {
        a4[i] = b64_lookup(a4[i]);
      }

      a4_to_a3(a3,a4);

      for (i = 0; i < 3; i++) {
        output[decLen++] = a3[i];
      }
      i = 0;
    }
  }

  if (i) {
    for (j = i; j < 4; j++) {
      a4[j] = '\0';
    }

    for (j = 0; j <4; j++) {
      a4[j] = b64_lookup(a4[j]);
    }

    a4_to_a3(a3,a4);

    for (j = 0; j < i - 1; j++) {
      output[decLen++] = a3[j];
    }
  }
  output[decLen] = '\0';
  return decLen;
}

int base64_enc_len(int plainLen) {
  int n = plainLen;
  return (n + 2 - ((n + 2) % 3)) / 3 * 4;
}

int base64_dec_len(char * input, int inputLen) {
  int i = 0;
  int numEq = 0;
  for(i = inputLen - 1; input[i] == '='; i--) {
    numEq++;
  }

  return ((6 * inputLen) / 8) - numEq;
}
////////////////////////////////////////////////


