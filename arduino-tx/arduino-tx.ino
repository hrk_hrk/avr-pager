#include <micro-ecc.h>
#include <AESLib.h>
#include <RadioHead.h>
#include <RH_ASK.h>
#include <RHDatagram.h>
#include <SPI.h> // Not actually used but needed to compile
#include <MemoryFree.h>
#include <string.h>
#include <pagerlib.h>

#define ADDRESS 5
#define NUM_ECC_DIGITS 24 //size of privkey, curvesize in bytes 
#define CURVE uECC_secp192r1()
#define MESSAGE_SIZE 32
#define PAGER_MESSAGE_SIZE 57

//#define MESSAGE_SIZE 64
//#define PAGER_MESSAGE_SIZE 99


RH_ASK driver(2000);
RHDatagram manager(driver, ADDRESS);

const struct uECC_Curve_t * curve = CURVE;
uint8_t sharedSecret[NUM_ECC_DIGITS];
uint8_t iv[16];

pl_pagermessage myMessage;

// uECC keys 192bit
uint8_t pubkey[NUM_ECC_DIGITS*2] = {0x67, 0xCF, 0x6C, 0x6F, 0x64, 0x19, 0xCC, 0xBF, 0x44, 0x60, 0x36, 0x2C, 0x99, 0x1D, 0x8C, 0x38, 0xFB, 0x6D, 0x18, 0xF2, 0x24, 0xEC, 0x8F, 0x8, 0xAB, 0x23, 0x76, 0xC, 0x4F, 0xA4, 0x63, 0x55, 0x26, 0xF8, 0x30, 0x9A, 0xA7, 0x6C, 0x55, 0x54, 0x10, 0x31, 0x0, 0xCA, 0x55, 0xF9, 0xC1, 0xFA};
uint8_t privkey[NUM_ECC_DIGITS+1] = {0xF6, 0x8B, 0x9B, 0x7F, 0xD9, 0xF8, 0xF7, 0x8C, 0x1, 0x1F, 0x6F, 0xF1, 0x9A, 0x6D, 0xC, 0xFC, 0xD4, 0x78, 0xBE, 0x26, 0x85, 0x78, 0xFD, 0xCA, 0xF4};

uint8_t remotePubkey[NUM_ECC_DIGITS*2] = {0x99, 0x61, 0xB5, 0x38, 0xB3, 0x83, 0x7E, 0xFB, 0xD9, 0x3F, 0x71, 0xA3, 0x81, 0x77, 0xB0, 0x48, 0x32, 0x29, 0x24, 0x6B, 0x76, 0x48, 0x9C, 0x7A, 0x70, 0xFD, 0x3F, 0xC4, 0xB8, 0xAB, 0x8E, 0xCD, 0x31, 0x88, 0x50, 0x2D, 0xE6, 0x53, 0x49, 0xE8, 0xC0, 0xB4, 0xB5, 0xC6, 0x4F, 0x97, 0x7F, 0x6B};

static int RNG(uint8_t *dest, unsigned size) {
  // Use the least-significant bits from the ADC for an unconnected pin (or connected to a source of 
  // random noise). This can take a long time to generate random data if the result of analogRead(0) 
  // doesn't change very frequently.
  while (size) {
    uint8_t val = 0;
    for (unsigned i = 0; i < 8; ++i) {
      int init = analogRead(0);
      int count = 0;
      while (analogRead(0) == init) {
        ++count;
      }
      
      if (count == 0) {
         val = (val << 1) | (init & 0x01);
      } else {
         val = (val << 1) | (count & 0x01);
      }
    }
    *dest = val;
    ++dest;
    --size;
  }
  // NOTE: it would be a good idea to hash the resulting random data using SHA-256 or similar.
  return 1;
}

uint8_t * generateIV()
{
    Serial.print("IV:");
    uint8_t iv[NUM_ECC_DIGITS];
    static uint8_t *ptr_iv = iv;
    for( int i=0 ; i < NUM_ECC_DIGITS; i++){
      RNG(ptr_iv, 1);
      Serial.print(iv[i] ,DEC);
      Serial.print(" ");
      ptr_iv++;
    }
    Serial.println("");
    return ptr_iv;
}


void hashSecret(uint8_t *p_secret)
{
    Serial.println("Secret:");
    for( int i=0; i < NUM_ECC_DIGITS; i++){
      Serial.print(p_secret[i]);
      Serial.print(" ");
    }
   Serial.println("");
}

char * encryptAES(char* p_data, uint8_t *p_key, uint8_t *p_iv)
{
//    Serial.println("encrypting with key:");
//    hashSecret(p_key);
//    uint8_t iv[] =  {0,1,2,3,4,5,6,7,122,9,10,11,12,13,14,15};
    aes192_cbc_enc(p_key, p_iv, p_data, 32);
    Serial.print("encrypted-cbc:");
    Serial.println(p_data);   
    return p_data;
}

char* decryptAES(char* p_data, uint8_t *p_key)
{
   uint8_t iv[] = {5,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
   aes_context ctx;
   ctx = aes192_cbc_dec_start(p_key, iv);
   aes192_cbc_dec_continue(ctx, p_data, 32);
   aes192_cbc_dec_finish(ctx);
//   Serial.print("decrypted-cbc:");
//   Serial.println(p_data);
   return p_data;
}

uint8_t *calcSharedSecret(uint8_t *p_pubkey, uint8_t *p_privkey)
{
     delay(100);
     if(uECC_valid_public_key(p_pubkey, curve) == 1){
//      Serial.println("Valid pubkey");
      uECC_shared_secret(p_pubkey, p_privkey, sharedSecret, curve);
//      hashSecret(sharedSecret);
    }
   //return sharedSecret;
   delay(100);
}

void sendPubkey()
{
    uint8_t compressedPubkey[NUM_ECC_DIGITS+1];
    uECC_compress(pubkey, compressedPubkey, curve);
    manager.setHeaderId(50);
    manager.sendto((uint8_t *)compressedPubkey, NUM_ECC_DIGITS+1, 2);
    manager.waitPacketSent();
    Serial.println("sending public key...");
}

void sendMessage(char* msg)
{
    Serial.println("Sending message...");
    manager.setHeaderId(51);
    manager.sendto((uint8_t *)msg, MESSAGE_SIZE, 2);
    manager.waitPacketSent();
}
void sendAll(char* msg)
{
//    Serial.println("sending");
    uint8_t pagerMsg[PAGER_MESSAGE_SIZE];
    uint8_t compressedPubkey[NUM_ECC_DIGITS+1];
    char *encData;
    //uint8_t *iv;
    uint8_t iv[] =  {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
    // uECC_compute_public_key(const uint8_t *private_key, uint8_t *public_key, uECC_Curve curve);
    uECC_compress(pubkey, compressedPubkey, curve); 
//    iv = generateIV();;
    encData = encryptAES(msg, sharedSecret, iv);
    for(int i=0; i < NUM_ECC_DIGITS+1; i++){ // put the compressed pubkey in the first 25 bytes
      pagerMsg[i] = compressedPubkey[i];
    }

    for(int i=NUM_ECC_DIGITS+1; i < NUM_ECC_DIGITS+33; i++){ // put the encrypted message in the next 32 bytes
      pagerMsg[i] = msg[i-(NUM_ECC_DIGITS+1)];
    }
    manager.setHeaderId(52);
    manager.sendto((uint8_t *)pagerMsg, PAGER_MESSAGE_SIZE, 2);
    manager.waitPacketSent();   
}

void messageInput()
{
   byte byteRead;
   char data[32];
   int i = 0;
   while( i < 32){
     if (Serial.available() > 0) {
       byteRead = Serial.read();
       if(byteRead==13){  // return presses, send message and break
        Serial.println();
        calcSharedSecret(remotePubkey, privkey);
        sendAll(data);
        break;
       }
       else
        data[i] = byteRead;
        Serial.write(byteRead);
        i++;
     }
   }
}

void setup()
{
    Serial.begin(9600);	  // Debugging only
    Serial.println("TX init");
    if (!manager.init())
         Serial.println("init failed");
    Serial.print("freeMemory()=");
    Serial.println(freeMemory());
}

void loop()
{
//    messageInput();
     Serial.println("start");   
     generateIV();
//    uint8_t iv[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
    char data[32] = "Hallo dit is een test";
    calcSharedSecret(remotePubkey, privkey);
    sendAll(data);
//    delay(2000);

}
